/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: main.cpp                            *
 * Description: Main file for yaxpg-cpp      *
 * Creation date: 2020-12-22                 *
 * Version: 1.0                              *
 *********************************************/

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "boost/program_options.hpp"
#include "csprng.hpp"
#include "string_manip.hpp"

#define CURRENT_VERSION "1.1"

using namespace std;
namespace po = boost::program_options;

/**
 * @brief Redefinition of vector<string> in the Wordlist type
 */
using Wordlist = vector<string>;

bool verbose_flag = true;

/**
 * @brief Class for a Passphrase
 */
class Passphrase {
public:
    /**
     * @brief Construct a new Passphrase object
     *
     * @param pre Prefix to use
     * @param suf Suffix to use
     * @param sep Separator to use
     */
    Passphrase(const string &pre, const string &suf,
               const string &sep) : _prefix{pre}, _suffix{suf}, _separator{sep} {}
    Passphrase()  = delete;
    ~Passphrase() = default;

    /**
     * @brief Adds a word to the Passphrase
     *
     * @param word The word to add
     */
    void add_word(string word) {_words.push_back(word);}

    /**
     * @brief toString converter for a Passphrase
     *
     * @return string The Passphrase transformed into a string
     */
    string toString() const {
        stringstream s_stream;
        s_stream << _prefix;
        for (auto i = 0; i < _words.size(); ++i) {
            s_stream << _words[i];
            if (i != _words.size() - 1) {
                s_stream << _separator;
            }
        }
        s_stream << _suffix;
        return s_stream.str();
    }

    /**
     * @brief Alternative toString to show the attributes of a Passphrase
     *
     * @return string
     */
    string toString_bis() const {
        stringstream s_stream;
        s_stream << "Prefix: "    << _prefix       << endl
                 << "Suffix: "    << _suffix       << endl
                 << "Separator: " << _separator    << endl
                 << "Nb words: "  << _words.size() << endl
                 << "Words: ";
        for (auto i = 0; i < _words.size(); ++i) {
            s_stream << _words[i];
            if (i != _words.size() - 1) {
                s_stream << ", ";
            }
        }
        s_stream << endl;
        return s_stream.str();
    }

private:
    string              _prefix    {""};
    string              _suffix    {""};
    string              _separator {""};
    vector<string>      _words;
};

/**
 * @brief Structure for the parameters used in the program
 */
struct Parameters {
    unsigned min_length {5};
    unsigned max_length {9};
    unsigned nb_words {4};
    unsigned case_type;
    unsigned nb_pass {5};
    unsigned prefix_length {2};
    unsigned suffix_length {2};
    unsigned separator_length {1};
    string   wordlist_file;
    string   output_file;
    string   digit_choice {"0123456789"};
    string   symbol_choice {"!@$^&*-_+{|~?/.;"};
    string   separator;
    string   prefix;
    string   suffix;
    string   config_in;
    string   config_out;
};

/**
 * @brief Prints the version on the program
 */
void version() {
    cout << "Yet Another XKCD Passphrase Generator (yaxpg-c) - version "
         << CURRENT_VERSION << endl << "Copyright (C) 2020 - Axel FEFEU"
         << endl << endl;
}

/**
 * @brief Reads a wordlist file
 *
 * @param filename The file to read from
 * @return Wordlist The wordlist
 */
Wordlist read_wordlist_file(const string &filename) {
    string word;
    fstream wl_file;
    Wordlist wl;
    wl_file.open(filename, ios::in);
    if (!wl_file) {
        cerr << "No such file" << endl;
        exit(EXIT_FAILURE);
    } else {
        for (; getline(wl_file, word);) {
            wl.push_back(word);
        }
    }
    wl_file.close();
    return wl;
}

/**
 * @brief Filters a wordlist by removing the words that don't have the right length
 *
 * @param wl          The wordlist to filter
 * @param min_length  The minimum length of words
 * @param max_length  The maximum length of words
 * @return Wordlist   The filtered wordlist
 */
Wordlist filter_wordlist_length(const Wordlist &wl, int min_length,
                                int max_length) {
    Wordlist filtered_wordlist;
    for (const string &current_word : wl) {
        if (current_word.length() >= min_length
                && current_word.length() <= max_length) {
            filtered_wordlist.push_back(current_word);
        }
    }
    return filtered_wordlist;
}

/**
 * @brief Generate padding of a certain length from a certain possible string
 *
 * @param pad_length The length of the padding
 * @param choice     The string to make the choice from
 * @return string    The padding string
 */
string generate_padding(unsigned pad_length, const string &choice) {
    duthomhas::csprng rng;
    stringstream s_stream;
    for (auto i = 0; i < pad_length; ++i) {
        size_t rnd_index = rng(size_t()) % choice.length();
        s_stream << choice[rnd_index];
    }
    return s_stream.str();
}

/**
 * @brief Generate an affix
 *
 * @param pad_length    The length of the padding (will be used for both the symbols and the digits)
 * @param is_prefix     Whether the affix to generate is a prefix or not
 * @param digit_choice  The string to make the choice of digits from
 * @param symbol_choice The string to make the choice of symbols from
 * @return string       The generated affix
 */
string generate_affix(unsigned pad_length, bool is_prefix,
                      const string &digit_choice,
                      const string &symbol_choice) {
    stringstream s_stream;
    string symbol_pad = generate_padding(pad_length, symbol_choice);
    string digit_pad  = generate_padding(pad_length, digit_choice);
    if (is_prefix) {
        s_stream << digit_pad << symbol_pad;
    } else {
        s_stream << symbol_pad << digit_pad;
    }
    return s_stream.str();
}

/**
 * @brief Selects a random word from a wordlist
 *
 * @param wl      The wordlist to select from
 * @return string The randomly selected word
 */
string select_random_word(const Wordlist &wl) {
    duthomhas::csprng rng;
    size_t rnd_index = rng(size_t()) % wl.size();
    return wl[rnd_index];
}

/**
 * @brief Builds a Passphrase from the given parameters
 *
 * @param wl        The wordlist the words will be selected from
 * @param nb_words  The number of words to select
 * @param prefix    The prefix used for the Passphrase
 * @param suffix    The suffix used for the Passphrase
 * @param separator The separator used for the Passphrase
 * @param case_type The case used for the selected words
 * @return Passphrase The built Passphrase
 */
Passphrase build_passphrase(const Wordlist &wl, unsigned nb_words,
                            string prefix, string suffix,
                            string separator, unsigned case_type) {
    Passphrase pp = Passphrase(prefix, suffix, separator);
    for (unsigned i = 0; i < nb_words; ++i) {
        string selected_word = select_random_word(wl);
        pp.add_word(change_case(selected_word, case_type));
    }
    return pp;
}

/**
 * @brief Gives the number of digits in a number
 *
 * @param n         The number to compute the number of digit of
 * @return unsigned The number of digits in the number
 */
unsigned nb_digits (unsigned n) {
    unsigned current = n;
    unsigned len     = 0;
    while (current != 0) {
        current /= 10;
        ++len;
    }
    return len;
}

/**
 * @brief Generates the Passphrases demanded
 *
 * @param params The parameters to used from the generation
 */
void generate_passphrases(Parameters params) {
    vector<Passphrase> vec_pp;
    Wordlist wl          = read_wordlist_file(params.wordlist_file);
    Wordlist filtered_wl = filter_wordlist_length(wl,
                           params.min_length, params.max_length);
    unsigned padding     = nb_digits(params.nb_pass);

    fstream output_file;
    if (params.output_file != "") {
        output_file.open(params.output_file, ios::out);
        if (!output_file) {
            cerr << "File not created";
        }
    }

    if (verbose_flag) {
        cout << "Generated passphrase" << (params.nb_pass == 1 ? "" : "s") << ":" <<
             endl <<
             endl;
    }

    for (unsigned i = 0; i < params.nb_pass; ++i) {
        string prefix    = params.prefix != "" ? params.prefix :
                           generate_affix(params.prefix_length, true,
                                          params.digit_choice, params.symbol_choice);
        string suffix    = params.suffix != "" ? params.suffix :
                           generate_affix(params.suffix_length, false,
                                          params.digit_choice, params.symbol_choice);
        string separator = params.separator != "" ? params.separator :
                           generate_padding(params.separator_length, params.symbol_choice);
        Passphrase generated_pp = build_passphrase(filtered_wl, params.nb_words,
                                  prefix, suffix, separator, params.case_type);
        vec_pp.push_back(generated_pp);

        if (verbose_flag) {
            stringstream s_stream;
            char index[padding];
            sprintf(index, "%0*d", padding, i + 1);
            s_stream << "Passphrase " << index << " (length "
                     << generated_pp.toString().length() << "):\t"
                     << generated_pp.toString();
            cout     << s_stream.str() << endl;
        }

        if (params.output_file != "") {
            if (output_file) {
                output_file << generated_pp.toString() << endl;
            }
        }
    }

    if (!output_file) {
        output_file.close();
    }
}

/**
 * @brief toString converter for the case type
 *
 * @param case_type The case_type to convert
 * @return string   The name of the case type
 */
string case_type_toString(unsigned case_type) {
    switch (case_type) {
    case 1:
        return "upper case";
        break;
    case 2:
        return "alternative case";
        break;
    case 3:
        return "random case";
        break;
    case 4:
        return "capitalize case";
        break;
    case 5:
        return "leet case";
        break;
    default:
        return "lower case";
    }
}

/**
 * @brief Prints the different parameters of the program
 *
 * @param params The parameters to print
 */
void print_params(Parameters params) {
    cout << "Parameters used:"
         << endl;
    cout << "\tMinimum length of words used:\t\t"
         << params.min_length
         << endl
         << "\tMaximum length of words used:\t\t"
         << params.max_length
         << endl
         << "\tNumber of words used:\t\t\t"
         << params.nb_words
         << endl
         << "\tCase type:\t\t\t\t"
         << case_type_toString(params.case_type)
         << endl;
    cout << "\tNumber of phrases to generate:\t\t"
         << params.nb_pass << endl
         << "\tLength of the prefix:\t\t\t"
         << params.prefix_length * 2 << endl
         << "\tLength of the suffix:\t\t\t"
         << params.suffix_length * 2 << endl
         << "\tLength of the separator:\t\t"
         << params.separator_length << endl;
    if (params.wordlist_file != "")
        cout << "\tWordlist filename:\t\t\t"
             << params.wordlist_file << endl;
    if (params.output_file != "")
        cout << "\tOutput filename:\t\t"
             << params.output_file   << endl;
    if (params.digit_choice != "")
        cout << "\tString used to generate digit padding:\t"
             << params.digit_choice  << endl;
    if (params.symbol_choice != "")
        cout << "\tString used to generate symbol padding:\t"
             << params.symbol_choice << endl;
    if (params.separator != "")
        cout << "\tOverridden separator:\t\t"
             << params.separator     << endl;
    if (params.prefix != "")
        cout << "\tOverridden prefix:\t\t"
             << params.prefix        << endl;
    if (params.suffix != "")
        cout << "\tOverridden suffix:\t\t"
             << params.suffix        << endl;
    if (params.config_in != "")
        cout << "\tInput configuration filename:\t\t"
             << params.config_in     << endl;
    if (params.config_out != "")
        cout << "\tOut configuration filename:\t\t"
             << params.config_out    << endl;
    cout << endl;
}

/**
 * @brief Initialize the parameters of the program
 *
 * @param argc The number of arguments given to the progam
 * @param argv The list of arguments given to the program
 * @return Parameters The parameters used in the program
 */
Parameters init_parameters(int argc, char **argv) {
    Parameters params;
    duthomhas::csprng rng;
    params.case_type = rng(unsigned()) % 5;

    po::options_description cmd_options("All options");
    cmd_options.add_options()
    ("help,h", "Show usage message and options")
    ("version,v", "Show the version of the program")
    ("min,m", po::value<unsigned>(&params.min_length), "Minimum length of words")
    ("max,M", po::value<unsigned>(&params.max_length), "Maximum length of words")
    ("nb_words,n", po::value<unsigned>(&params.nb_words), "Number of words")
    ("case,c", po::value<unsigned>(&params.case_type),
     "Type of case\nNote: \tThe possible values are from 0 to 5 that respectively mean lower, upper, alternative, random, capitalize or leet case")
    ("nb_pass,N", po::value<unsigned>(&params.nb_pass), "Number of passphrases")
    ("prefix_length,p", po::value<unsigned>(&params.prefix_length),
     "Length of the prefix\nNote: \tthe value specified will generate that much digits and symbols for the prefix")
    ("suffix_length,s", po::value<unsigned>(&params.suffix_length),
     "Length of the suffix\nNote: \tthe value specified will generate that much digits and symbols for the suffix")
    ("separator_length,S", po::value<unsigned>(&params.separator_length),
     "Length of the separator")
    ("wordlist,w", po::value<string>(&params.wordlist_file),
     "Filename of the wordlist to use")
    ("output,o", po::value<string>(&params.output_file),
     "Filename of an output file")
    ("read-config,r", po::value<string>(&params.config_in),
     "Filename of the config file to read")
    ("set-digit_choice", po::value<string>(&params.digit_choice),
     "Overrides the string used to generate the number used in affixes")
    ("set-symbol_choice", po::value<string>(&params.symbol_choice),
     "Overrides the string used to generate the symbols used in affixes and separator")
    ("set-prefix", po::value<string>(&params.prefix), "Overrides the prefix")
    ("set-suffix", po::value<string>(&params.suffix), "Overrides the suffix")
    ("set-separator", po::value<string>(&params.separator),
     "Overrides the separator")
    ("save-config", po::value<string>(&params.config_out),
     "Filename of the config file to write")
    ("brief", "Doesn't print the password outputs\nRecommended use: \twith an output file");

    po::positional_options_description pos_options;
    pos_options.add("wordlist", -1);
    po::variables_map var_map;
    store(po::command_line_parser(argc,
                                  argv).options(cmd_options).positional(pos_options).run(), var_map);
    notify(var_map);

    if (params.config_in != "") {
        ifstream if_stream(params.config_in.c_str());
        if (!if_stream) {
            cerr << "Couldn't open :" << params.config_in << endl;
        } else {
            store(parse_config_file(if_stream, cmd_options), var_map);
            notify(var_map);
        }
    }

    if (var_map.count("help")) {
        cerr << "Usage " << argv[0] << " wordfile [OPTIONS]" << endl;
        cerr << cmd_options << endl;
        exit(EXIT_FAILURE);
    }

    if (var_map.count("version")) {
        version();
    }

    if (var_map.count("brief")) {
        verbose_flag = false;
    }

    if (params.wordlist_file == "") {
        cerr << cmd_options << endl;
        exit(EXIT_FAILURE);
    }

    return params;
}

/**
 * @brief Saves the parameters into a ini file
 *
 * @param params The parameters to save
 */
void save_param(Parameters params) {
    fstream config_out;
    if (params.config_out != "") {
        config_out.open(params.config_out, ios::out);
        if (!config_out) {
            cerr << "File not created" << endl;
        }
    }
    if (params.config_out != "") {
        if (config_out) {
            cout << "Saving config to " << params.config_out << endl;
            config_out << "min" << "=" << params.min_length << endl;
            config_out << "max" << "=" << params.max_length << endl;
            config_out << "nb_words" << "=" << params.nb_words << endl;
            config_out << "case" << "=" << params.case_type << endl;
            config_out << "nb_pass" << "=" << params.nb_pass << endl;
            config_out << "prefix_length" << "=" << params.prefix_length << endl;
            config_out << "suffix_length" << "=" << params.suffix_length << endl;
            config_out << "separator_length" << "=" << params.separator_length << endl;
            config_out << "wordlist" << "=" << params.wordlist_file << endl;
            config_out << "output" << "=" << params.output_file << endl;
            config_out << "read-config" << "=" << params.config_in << endl;
            config_out << "set-digit_choice" << "=" << params.digit_choice << endl;
            config_out << "set-symbol_choice" << "=" << params.symbol_choice << endl;
            config_out << "set-prefix" << "=" << params.prefix << endl;
            config_out << "set-suffix" << "=" << params.suffix << endl;
            config_out << "set-separator" << "=" << params.separator << endl;
            config_out << "save-config" << "=" << params.config_out << endl;
            config_out.close();
        }
    }
}

int main(int argc, char **argv) {
    Parameters params = init_parameters(argc, argv);

    print_params(params);
    generate_passphrases(params);
    save_param(params);

    return EXIT_SUCCESS;
}