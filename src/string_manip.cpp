/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: string_manip.hpp                    *
 * Description: Header for the string_manip  *
 * Creation date: 2020-12-22                 *
 *********************************************/

#include <sstream>
#include <iostream>
#include <algorithm>

#include "string_manip.hpp"
#include "csprng.hpp"

#define LOWER_CASE       0
#define UPPER_CASE       1
#define ALTERNATIVE_CASE 2
#define RANDOM_CASE      3
#define CAPITALIZE_CASE  4
#define LEET_CASE        5

std::string to_lower_case(std::string word) {
    std::transform(word.begin(), word.end(),
    word.begin(), [](unsigned char c) {return std::tolower(c);});
    return word;
}

std::string to_upper_case(std::string word) {
    std::transform(word.begin(), word.end(),
    word.begin(), [](unsigned char c) {return std::toupper(c);});
    return word;
}

std::string to_alternative_case(std::string word) {
    unsigned i = 0;
    std::transform(word.begin(), word.end(),
    word.begin(), [&i](unsigned char c) {unsigned char x = i % 2 == 0 ? std::tolower(c) : std::toupper(c); ++i; return x;});
    return word;
}

std::string to_random_case(std::string word) {

    std::transform(word.begin(), word.end(),
    word.begin(), [](unsigned char c) {duthomhas::csprng rng; return rng(unsigned()) % 2 == 0 ? std::tolower(c) : std::toupper(c);});
    return word;
}

std::string to_capitalize_case(std::string word) {
    unsigned i = 0;
    std::transform(word.begin(), word.end(),
    word.begin(), [&i](unsigned char c) {unsigned char x = i != 0 ? std::tolower(c) : std::toupper(c); ++i; return x;});
    return word;
}

char to_leet(char letter) {
    switch (letter) {
    case 'A': case 'a':
        return '4';
        break;
    case 'B': case 'b':
        return '8';
        break;
    case 'E': case 'e':
        return '3';
        break;
    case 'O': case 'o':
        return '0';
        break;
    case 'S': case 's':
        return '5';
        break;
    case 'T': case 't':
        return '7';
        break;
    case 'Z': case 'z':
        return '2';
        break;
    default:
        return letter;
    }
    return letter;
}

std::string to_leet_case(std::string word) {
    std::transform(word.begin(), word.end(),
    word.begin(), [](unsigned char c) {return to_leet(c);});
    return word;
}

std::string change_case(std::string word, unsigned case_type) {
    switch (case_type % 6) {
    case LOWER_CASE:
        return to_lower_case(word);
        break;
    case UPPER_CASE:
        return to_upper_case(word);
        break;
    case ALTERNATIVE_CASE:
        return to_alternative_case(word);
        break;
    case RANDOM_CASE:
        return to_random_case(word);
        break;
    case CAPITALIZE_CASE:
        return to_capitalize_case(word);
        break;
    case LEET_CASE:
        return to_random_case(to_leet_case(word));
        break;
    default:
        return to_lower_case(word);
    }
    return word;
}