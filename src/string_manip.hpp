/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: string_manip.hpp                    *
 * Description: Header for the string_manip  *
 * Creation date: 2020-12-22                 *
 * Version: 0.1                              *
 *********************************************/

#ifndef YAXPG_STRING_MANIP_HEADER__
#define YAXPG_STRING_MANIP_HEADER__

#include <string>

/**
 * @brief Converts a string to lower case
 *
 * @param word         The word to convert
 * @return std::string The converted word
 */
std::string to_lower_case(std::string word);

/**
 * @brief Converts a string to upper case
 *
 * @param word         The word to convert
 * @return std::string The converted word
 */
std::string to_upper_case(std::string word);

/**
 * @brief Converts a string to random case
 *
 * @param word         The word to convert
 * @return std::string The converted word
 */
std::string to_random_case(std::string word);

/**
 * @brief Converts a string to capitalize case
 *
 * @param word         The word to convert
 * @return std::string The converted word
 */
std::string to_capitalize_case(std::string word);

/**
 * @brief Converts a letter to its leet interpretation
 *
 * Only changes A, B, E, O, S, T, Z, a, b, e, o, s, t & z
 *
 * @param letter The letter to convert
 * @return char  The converted letter
 */
char to_leet(char letter);

/**
 * @brief Converts a string to a leet case
 *
 * @param word         The word to convert
 * @return std::string The converted word
 */
std::string to_leet_case(std::string word);


/**
 * @brief Converts a string to a given case
 *
 * @param word         The word to convert
 * @param case_type    The case to convert to
 * @return std::string The converted word
 */
std::string change_case(std::string word, unsigned case_type);

#endif