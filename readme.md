# Yet Another XKCD Passphrase Generator in C++ – yaxpg-cpp

Generate [xkcd](https://xkcd.com/936/)-style passphrase like [xkpasswd](https://xkpasswd.net/s/) but locally using C++.

# Requires

- [Boost.Program_options](https://www.boost.org/doc/libs/1_75_0/doc/html/program_options.html)
    - On Ubuntu distributions, you can get it with `apt install libboost-program-options-dev`

# How to use

Inside the cloned repository, to compile the program:

```shell
mkdir build
cd build
cmake ..
make
```

To get the usage message and the list of possible options, launch the program with `--help` or `-h`:

```shell
./yaxpg-cpp --help
```

To launch the program, you need at least one arguments which is a path to a wordlist file, some are provided in this repository ([wordlist_english](./wordlist_english), [wordlist_french](./wordlist_french), [wordlist_latin](./wordlist_latin)). Example:

```shell
./yaxpg-cc -w ../wordlist_english
```

Alternatively you can use a ini file to set your parameters and launch the program with `-r` (or `--read-config`). Example:

```shell
./yaxpg-cc -r ../example.ini
```

## Documentation

To generate the documentation, you'll need Doxygen and $\LaTeX$ installed. If that's the case all you have to do is execute the command `make doc_doxygen` while inside the previously created `build` folder:

This will generate a `doc` at the root of the repository in which there are two directories in which you'll respectively find the documentation in html and $\LaTeX$ form.

Alternatively you could use `make doc_pdf` which will run the previous command as well as the generation of a pdf from the $\LaTeX$ sources.

# Checklist

- [x] Read wordlist
- [x] Store appropriate words
- [x] Select random words
- [x] Concat words with separator
- [x] Concat passphrase with affixes
- [x] Display passphrase
- [x] Generate multiple passphrases
- [x] Change cases of words
- [x] Manage launch options
- [x] Read ini config file
- [x] Save config to ini file

# Uses

- [https://github.com/Duthomhas/CSPRNG](https://github.com/Duthomhas/CSPRNG)
- [https://www.boost.org/](https://www.boost.org/)